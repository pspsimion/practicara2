package com.simion.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Created by dam on 22/02/17.
 */
public class Cliente {

    private String nick;
    private char[] passwd;
    private Socket socket;
    public BufferedReader entrada;
    public PrintWriter salida;


    public Cliente(Socket socketCliente) throws IOException {
        this.socket = socketCliente;
        iniciar();
    }

    private void iniciar() throws IOException {
        this.entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.salida = new PrintWriter(socket.getOutputStream(),true);
    }


    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public char[] getPasswd() {
        return passwd;
    }

    public void setPasswd(char[] passwd) {
        this.passwd = passwd;
    }

    public boolean estaConnected(){
        return socket.isConnected();
    }

    @Override
    public String toString() {
        return nick;
    }
}

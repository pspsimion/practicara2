package com.simion;

import com.sun.org.apache.regexp.internal.RE;
import com.sun.xml.internal.fastinfoset.tools.FI_DOM_Or_XML_DOM_SAX_SAXEvent;

import javax.swing.*;
import java.io.BufferedReader;
import java.net.Socket;

import static com.simion.util.Constantes.*;

/**
 * Created by dam on 22/02/17.
 */
public class TareaEscuchar extends SwingWorker<Void, Void>{

    private Socket socket;
    private BufferedReader entrada;

    public TareaEscuchar(Socket socket, BufferedReader entrada){
        this.socket = socket;
        this.entrada = entrada;
    }

    @Override
    protected Void doInBackground() throws Exception {
        while (socket.isConnected()){
            String mensaje = entrada.readLine();
            if(mensaje != null){
                if (mensaje.equals(LOGINOK)){
                    firePropertyChange(LOGINOK, null, mensaje);
                }
                else if (mensaje.equals(ERRORPASSWD)){
                    firePropertyChange(ERRORPASSWD, null, mensaje);
                }
                else if (mensaje.equals(NOREGISTRADO)){
                    firePropertyChange(NOREGISTRADO, null, mensaje);
                }
                else if (mensaje.equals(REGISTRADO)){
                    firePropertyChange(REGISTRADO, null, mensaje);
                }
                else if (mensaje.equals(SINGUPOK)){
                    firePropertyChange(SINGUPOK, null, mensaje);
                }
                else if (mensaje.equals(CONECTADO)){
                    firePropertyChange(CONECTADO, null, mensaje);
                }
                else if (mensaje.startsWith(NICKS)){
                    firePropertyChange(NICKS, null, mensaje);
                }
                else if (mensaje.startsWith(CHATPRIVADO)){
                    firePropertyChange(CHATPRIVADO,null, mensaje);
                }
                else if (mensaje.startsWith(CHATPRIVADONO)){
                    firePropertyChange(CHATPRIVADONO, null, mensaje);
                }
                else {
                    firePropertyChange(MENSAJE, null, mensaje);
                }
            }
        }

        return null;
    }
}

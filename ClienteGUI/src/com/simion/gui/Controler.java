package com.simion.gui;

import com.simion.TareaEscuchar;
import com.simion.base.Cliente;
import com.simion.util.Pestania;
import com.simion.util.Util;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ConnectException;
import java.net.Socket;

import static com.simion.util.Constantes.*;

/**
 * Created by dam on 22/02/17.
 * Controlador del programa.
 */
public class Controler implements PropertyChangeListener, KeyListener, ActionListener, ChangeListener {

    private static Logger logger = LogManager.getLogger(Controler.class);

    private Ventana view;
    private TareaEscuchar tareaEscuchar;
    private  Cliente cliente;
    private Socket socket;
    private LoginDialog login;
    private boolean servidorConectado;
    private String nickRemoto;

    public Controler(Ventana view) {
        this.view = view;
        boolean error = true;
        while (error){
            try {
                ConfigDialog conf = new ConfigDialog();
                conf.setVisible(true);
                socket = new Socket(conf.getServidor(), conf.getPuerto());
                cliente = new Cliente(socket);
                this.tareaEscuchar = new TareaEscuchar(socket, cliente.entrada);
                error = false;
                servidorConectado = true;
            }catch (ConnectException ce){
                Util.warning("El servidor esta desconectado", "Servidor desconecado");
                logger.trace("Error de conexion con el servidor.");
                StringWriter sw = new StringWriter();
                ce.printStackTrace(new PrintWriter(sw));
                logger.error(sw);
                error = true;
            } catch (IOException e) {
                Util.warning("Error de entrada salida", "Error IOException");
                logger.trace("Error de entrada salida");
                StringWriter sw = new StringWriter();
                e.printStackTrace(new PrintWriter(sw));
                logger.error(sw);
                error = true;
            }
        }
        SwingWorker<Void, Void> swingWorker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                while (!socket.isClosed()){
                    if (!servidorConectado){
                        view.lbServidor.setText("Servidor: DESCONECTADO");
                        view.lbServidorChat.setText("Servidor: DESCONECTADO");
                    }
                    cliente.salida.println(CONECTADO);
                    servidorConectado = false;
                    Thread.sleep(5000);
                }
                return null;
            }
        };
        swingWorker.execute();
        addListeners();
        login = new LoginDialog();
        enviarLogin();
    }

    private void enviarLogin() {
        login.setVisible(true);
        cliente.setNick(login.getNick());
        cliente.setPasswd(login.getPasswd());
        String passwd = "";
        for (char caracter:login.getPasswd()){
            passwd += caracter;
        }
        if (login.getPestania().equals(Pestania.LOGIN)){
            cliente.salida.println(LOGINOK+"#"+login.getNick()+"#"+passwd);
        }
        else if (login.getPestania().equals(Pestania.SINGUP)){
            cliente.salida.println(SINGUPOK+"#"+login.getNick()+"#"+passwd);
        }
    }

    private void addListeners() {
        tareaEscuchar.addPropertyChangeListener(this);
        tareaEscuchar.execute();
        view.tfMensaje.addKeyListener(this);
        view.tfMensajeChat.addKeyListener(this);
        view.btEnviar.addActionListener(this);
        view.btEnviarChat.addActionListener(this);
        view.frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                cliente.salida.println(SALIR);
                System.exit(0);
            }
        });
        view.pnlPestaniero.addChangeListener(this);
        view.btChat.addActionListener(this);
        view.btIgnorar.addActionListener(this);
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getPropertyName().equals(LOGINOK)){
            Util.warning("Login correcto", "Login correcto");
            view.frame.setTitle("Chat de "+cliente.getNick());
            view.lbNick.setText(cliente.getNick());
            view.lbServidorChat.setText(cliente.getNick());
            logger.trace("Usuario logeado con exito.");
        }
        else if (event.getPropertyName().equals(ERRORPASSWD)){
            Util.warning("Contraseña introducida es incorrecta.", "Error loggin");
            logger.warn("Usuario ha introducido mal la contraseña");
            enviarLogin();
        }
        else if (event.getPropertyName().equals(NOREGISTRADO)){
            Util.warning("Usuario no registrado.", "Usuario no registrado");
            logger.warn("Usuario ha introducido un nick no registrado.");
            enviarLogin();
        }
        else if (event.getPropertyName().equals(REGISTRADO)){
            Util.warning("Usuario ya registrado.", "Usuario ya registrado");
            logger.warn("Usuario ha introducido un nick ya registrado.");
            enviarLogin();
        }
        else if (event.getPropertyName().equals(SINGUPOK)){
            Util.warning("El usuario se ha registrado con exito", "Registro correcto");
            logger.trace("Usuario registrado con exito.");
            view.frame.setTitle("Chat de "+cliente.getNick());
            view.lbNick.setText(cliente.getNick());
            view.lbServidorChat.setText(cliente.getNick());
        }
        else if (event.getPropertyName().equals(MENSAJE)){
            String mensaje = (String) event.getNewValue();
            view.taChat.append(mensaje+"\n");
        }
        else if (event.getPropertyName().equals(CONECTADO)){
            view.lbServidor.setText("Servidor: CONECTADO");
            view.lbServidorChat.setText("Servidor: CONECTADO");
            servidorConectado = true;
        }
        else if (event.getPropertyName().equals(NICKS)){
            view.dlmUsuarios.removeAllElements();
            String mensaje = (String) event.getNewValue();
            String[] partes = mensaje.split("#");
            for (int i = 1; i<partes.length; i++){
                view.dlmUsuarios.addElement(partes[i]);
            }
        }
        else if (event.getPropertyName().equals(CHATPRIVADO)) {
            String titulo;
            boolean chatExiste = false;
            int indiceActual = view.pnlPestaniero.getSelectedIndex();
            String mensaje = (String) event.getNewValue();
            String[] partes = mensaje.split("#");
            for (int i = 0; i < view.pnlPestaniero.getTabCount(); i++){
                titulo = view.pnlPestaniero.getTitleAt(i);
                if (titulo.equals(partes[1])){
                    view.pnlPestaniero.setSelectedIndex(i);
                    view.dlmUsuariosChat.removeAllElements();
                    view.dlmUsuariosChat.addElement(cliente.getNick());
                    view.dlmUsuariosChat.addElement(partes[1]);
                    view.taChatChat.append(partes[2]+"\n");
                    chatExiste = true;
                    break;
                }
            }
            if (!chatExiste){
                String nick = partes[1];
                view.pnlPestaniero.addTab(nick, view.pnlChat);
                int numPestanias = view.pnlPestaniero.getTabCount();
                view.lbNickChat.setText(cliente.getNick());
                view.pnlPestaniero.setSelectedIndex(numPestanias-1);
                view.dlmUsuariosChat.removeAllElements();
                view.dlmUsuariosChat.addElement(cliente.getNick());
                view.dlmUsuariosChat.addElement(nick);
                view.taChatChat.append(partes[2]+"\n");
            }
            view.pnlPestaniero.setSelectedIndex(indiceActual);
        }
        else if (event.getPropertyName().equals(CHATPRIVADONO)){
            view.taChatChat.append("El usuario no esta conectado."+"\n");
        }
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyChar() == KeyEvent.VK_ENTER){
            if (view.pnlPestaniero.getSelectedIndex() == 0){
                cliente.salida.println(view.tfMensaje.getText());
                view.tfMensaje.setText("");
            }
            else{
                cliente.salida.println(CHATPRIVADO+"#"+nickRemoto+"#"+view.tfMensajeChat.getText());
                view.tfMensajeChat.setText("");
            }
            logger.trace("Se ha enviado un mensaje");
        }

    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getActionCommand().equals("Enviar")){
            if (view.pnlPestaniero.getSelectedIndex() == 0){
                cliente.salida.println(view.tfMensaje.getText());
            }
            else{
                cliente.salida.println(CHATPRIVADO+"#"+nickRemoto+"#"+view.tfMensajeChat.getText());
            }
            view.tfMensaje.setText("");
            logger.trace("Se ha enviado un mensaje");
        }
        else if (actionEvent.getActionCommand().equals("Chat")){
            if (!view.lsUsuarios.isSelectionEmpty()){
                String nickSeleccionado = view.dlmUsuarios.get(view.lsUsuarios.getSelectedIndex());
                if (nickSeleccionado.equals(cliente.getNick())){
                    Util.warning("No puedes crear un chat contigo mismo", "Chat Propio");
                }
                else{
                    view.pnlPestaniero.addTab(nickSeleccionado, view.pnlChat);
                    int numPestanias = view.pnlPestaniero.getTabCount();
                    view.lbNickChat.setText(cliente.getNick());
                    view.pnlPestaniero.setSelectedIndex(numPestanias-1);
                    nickRemoto = view.pnlPestaniero.getTitleAt(numPestanias-1);
                }
            }
            else
                Util.warning("Primero debe seleccionar un usuario.", "Seleccionar usuarios.");

        }
        else if (actionEvent.getActionCommand().equals("Ignorar")){
            if (view.lsUsuariosChat.isSelectionEmpty()){
                Util.warning("Primero debe seleccionar un usuario.", "Seleccionar usuario.");
            }
            else {
                String nickSeleccionado = view.dlmUsuariosChat.get(view.lsUsuariosChat.getSelectedIndex());
                cliente.salida.println(IGNORAR+"#"+nickSeleccionado);
                view.btIgnorar.setText("No Ignorar");
            }
        }
        else if (actionEvent.getActionCommand().equals("No Ignorar")){
            if (view.lsUsuarios.isSelectionEmpty()){
                Util.warning("Primero debe seleccionar un usuario.", "Seleccionar usuario.");
            }
            else {
                String nickSeleccionado = view.dlmUsuariosChat.get(view.lsUsuariosChat.getSelectedIndex());
                cliente.salida.println(NOIGNORAR+"#"+nickSeleccionado);
                view.btIgnorar.setText("Ignorar");
            }
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        int indiceSeleccionado = view.pnlPestaniero.getSelectedIndex();
        if (indiceSeleccionado >= 2){
            nickRemoto = view.pnlPestaniero.getTitleAt(indiceSeleccionado);
            view.dlmUsuariosChat.removeAllElements();
            view.dlmUsuariosChat.addElement(cliente.getNick());
            view.dlmUsuariosChat.addElement(nickRemoto);
        }
    }
}

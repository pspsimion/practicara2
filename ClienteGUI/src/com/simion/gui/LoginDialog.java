package com.simion.gui;

import com.simion.util.Pestania;
import com.simion.util.Util;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.*;

public class LoginDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTabbedPane pnlPestanias;
    private JTextField tfNick;
    private JPasswordField pfPasswd;
    private JPanel pnlIniciarSesion;
    private JPanel pnlRegistrar;
    private JTextField tfNickR;
    private JPasswordField pfPasswd1R;
    private JPasswordField pfPasswd2R;

    private String nick;
    private char[] passwd;
    private Pestania pestania;



    public LoginDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        pack();
        setLocationRelativeTo(null);

        pestania = Pestania.LOGIN;

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        pnlPestanias.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                if (pnlPestanias.getSelectedIndex() == 0){
                    pestania = Pestania.LOGIN;
                }
                else if (pnlPestanias.getSelectedIndex() == 1){
                    pestania = Pestania.SINGUP;
                }
            }
        });
    }

    private void onOK() {
        if (pestania.equals(Pestania.LOGIN)){
            nick = tfNick.getText();
            passwd = pfPasswd.getPassword();
        }
        else if (pestania.equals(Pestania.SINGUP)){
            nick = tfNickR.getText();
            char[] pass1 = pfPasswd1R.getPassword();
            char[] pass2 = pfPasswd2R.getPassword();
            String mensaje = "Las contraseñas no coinciden";
            String titulo = "Contraseñas no coinciden.";
            if (pass1.length == pass2.length){
                for (int i = 0; i<pass1.length; i++){
                    if (pass1[i] != pass2[i]){
                        Util.warning(mensaje, titulo);
                        return;
                    }
                }
            }
            else {
                Util.warning(mensaje, titulo);
                return;
            }
            passwd = pass1;
        }
        dispose();
    }

    private void onCancel() {
        System.exit(0);
        dispose();
    }

    public static void main(String[] args) {
        LoginDialog dialog = new LoginDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public Pestania getPestania() {
        return pestania;
    }

    public String getNick(){
        return nick;
    }

    public char[] getPasswd(){
        return passwd;
    }
}

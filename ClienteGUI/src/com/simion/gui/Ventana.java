package com.simion.gui;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by dam on 22/02/17.
 */
public class Ventana extends JFrame{

    JPanel pnlPrincipal;
     JTabbedPane pnlPestaniero;
     JPanel pnlLobby;
     JTextArea taChat;
     JTextField tfMensaje;
     JList lsUsuarios;
     JLabel lbNick;
     JPanel pnlAyuda;
     JTextArea taAyuda;
     JButton btEnviar;
    JLabel lbServidor;
    JButton btChat;
    JPanel pnlChat;
    JLabel lbNickChat;
    JTextArea taChatChat;
    JTextField tfMensajeChat;
    JList lsUsuariosChat;
    JButton btEnviarChat;
    JLabel lbServidorChat;
    JButton btIgnorar;
    JFrame frame;

     DefaultListModel<String> dlmUsuarios;
     DefaultListModel<String> dlmUsuariosChat;

    public Ventana() {
        frame = new JFrame("Chat");
        frame.setContentPane(pnlPrincipal);
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        pnlPestaniero.remove(1);
        frame.setVisible(true);

        dlmUsuarios = new DefaultListModel<>();
        dlmUsuariosChat = new DefaultListModel<>();
        lsUsuarios.setModel(dlmUsuarios);
        lsUsuariosChat.setModel(dlmUsuariosChat);
    }
}

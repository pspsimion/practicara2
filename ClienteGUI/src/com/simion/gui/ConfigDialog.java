package com.simion.gui;

import com.simion.util.Util;

import javax.swing.*;
import java.awt.event.*;

public class ConfigDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfServidor;
    private JTextField tfPuerto;

    private String servidor;
    private int puerto;

    public ConfigDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        pack();
        setLocationRelativeTo(null);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        servidor = tfServidor.getText();
        try{
            puerto = Integer.parseInt(tfPuerto.getText());
        }catch (NumberFormatException nfe){
            Util.warning("El puerto introducido no tiene un formato correcto.", "Puerto incorrecto");
            return;
        }
        dispose();
    }

    private void onCancel() {
        System.exit(0);
        dispose();
    }

    public static void main(String[] args) {
        ConfigDialog dialog = new ConfigDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public String getServidor(){
        return servidor;
    }

    public int getPuerto(){
        return puerto;
    }
}

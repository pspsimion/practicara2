package com.simion.util;

/**
 * Created by dam on 27/02/17.
 */
public class Constantes {

    public static final String LOGINOK = "/LoginOK";
    public static final String ERRORPASSWD = "/errorPasswd";
    public static final String NOREGISTRADO = "/NoRegistrado";
    public static final String REGISTRADO = "/Registrado";
    public static final String SINGUPOK = "/SingUpOK";
    public static final String MENSAJE = "/MensajE";
    public static final String SALIR = "/SaliR";

    public static final String IGNORAR = "/ignorar";
    public static final String NOIGNORAR = "/noIgnorar";
    public static final String CHATPRIVADO = "/ChatPrivadO";
    public static final String CHATPRIVADONO = "/ChatPrivadoNO";
    public static final String CONECTADO = "/conectado?";
    public static final String NICKS = "/NickS";
}

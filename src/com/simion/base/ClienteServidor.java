package com.simion.base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by dam on 22/02/17.
 */
public class ClienteServidor {

    private String nick;
    private String passwd;
    private Socket socket;


    public BufferedReader entrada;
    public PrintWriter salida;
    public ArrayList<ClienteServidor> ignorados;


    public ClienteServidor(Socket socketCliente) throws IOException {
        this.socket = socketCliente;
        iniciar();
    }

    private void iniciar() throws IOException {
        this.entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.salida = new PrintWriter(socket.getOutputStream(), true);
        this.ignorados = new ArrayList<>();

    }

    public void setSocket(Socket socket){
        this.socket = socket;
    }

    public Socket getSocket(){
        return this.socket;
    }
    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public boolean estaConectado(){
        return !socket.isClosed();
    }

    @Override
    public String toString() {
        return nick;
    }

    public void desconectar() throws IOException {

        socket.close();
    }
}

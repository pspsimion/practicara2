package com.simion.base;

import java.io.IOException;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import static com.simion.util.Constantes.CHATPRIVADO;
import static com.simion.util.Constantes.NICKS;

/**
 * Created by dam on 22/02/17.
 */
public class Servidor {

    private int puerto;
    private ServerSocket socket;
    private ArrayList<ClienteServidor> clientes;
    private boolean conectado;

    public Servidor(int puerto){
        this.puerto = puerto;
        conectado = false;
    }

    public void iniciar() throws IOException {
        clientes = new ArrayList<>();
        socket = new ServerSocket(puerto);
        conectado = true;
    }

    public void desconectar() throws IOException {
        socket.close();
        conectado = false;
        System.out.println("Servidor desconectado");
    }

    public boolean estaConectado() {
        return conectado;
    }

    public Socket aceptarConexion() throws IOException {
        return socket.accept();
    }

    public ArrayList<ClienteServidor> getClientes() {
        return clientes;
    }

    public void enviarMensaje(String mensaje, ClienteServidor clienteServidor) {
        for (ClienteServidor cliente: clientes){
            if (!cliente.getNick().equals(clienteServidor.getNick())){
                System.out.println();
                if (!cliente.ignorados.contains(clienteServidor)){
                    cliente.salida.println(clienteServidor.getNick()+" > "+mensaje);
                }
            }
            else if (cliente.getNick().equals(clienteServidor.getNick())){
                clienteServidor.salida.println(" Yo > "+mensaje);
            }
        }
    }

    public void enviarMensaje(String mensaje) {
        for (ClienteServidor cliente:clientes){
            if (cliente.estaConectado()){
                cliente.salida.println(mensaje);
            }
        }
    }

    public boolean enviarMensajePrivado(String nickRemoto, String mensaje, ClienteServidor clienteServidorYo) {
        boolean enviado = false;
        for (ClienteServidor cliente:clientes){
            if (cliente.estaConectado() && cliente.getNick().equals(nickRemoto)){
                if (!cliente.ignorados.contains(clienteServidorYo)){
                    cliente.salida.println(CHATPRIVADO+"#"+clienteServidorYo.getNick()+"#"+clienteServidorYo.getNick()+" > "+mensaje);
                }
                enviado =  true;
            }
            else if (cliente.getNick().equals(clienteServidorYo.getNick())){
                cliente.salida.println(CHATPRIVADO+"#"+nickRemoto+"#"+" Yo > "+mensaje);
            }
        }
        return enviado;
    }

    public void aniadirCliente(ClienteServidor clienteServidor) {
        clientes.add(clienteServidor);
    }

    public ClienteServidor getCliente(String nick) {
        for (ClienteServidor cliente: clientes){
            if (cliente.getNick().equals(nick)){
                return cliente;
            }
        }
        return null;
    }

    public void enviarNicks() {
        StringBuilder sb = new StringBuilder();
        sb.append(NICKS);
        for (ClienteServidor cliente:clientes){
            if (cliente.estaConectado()){
                sb.append("#");
                sb.append(cliente.getNick());
            }
        }

        enviarMensaje(sb.toString());
    }
}

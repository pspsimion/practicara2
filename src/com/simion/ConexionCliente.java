package com.simion;

import com.simion.base.ClienteServidor;
import com.simion.base.Servidor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import static com.simion.util.Constantes.*;

/**
 * Created by dam on 22/02/17.
 */
public class ConexionCliente extends Thread{

    private Servidor servidor;
    private ClienteServidor clienteServidor;

    private static final Logger logger = LogManager.getLogger(ClienteServidor.class);

    public ConexionCliente(Servidor servidor, ClienteServidor clienteServidor) {
        this.servidor = servidor;
        this.clienteServidor = clienteServidor;
    }

    @Override
    public void run() {
        try{
            String orden;
            String partes[];
            while (clienteServidor.estaConectado()){
                orden = clienteServidor.entrada.readLine();
                partes = orden.split("#");
                if (partes.length == 2){
                    String[] temp = partes;
                    partes = new String[3];
                    partes[0] = temp[0];
                    partes[1] = temp[1];
                    partes[2] = "";
                }
                ClienteServidor ignorado;
                switch (partes[0]){
                    case LOGINOK:
                        logger.trace("Cliente ha enviado solicitud de login");
                        ArrayList<ClienteServidor> lista = servidor.getClientes();
                        if (servidor.getClientes().size()==0||!lista.contains(servidor.getCliente(partes[1]))){
                            this.clienteServidor.salida.println(NOREGISTRADO);
                            logger.warn("Loggin incorrecto. Usuario no registrado.");
                            break;
                        }
                        for (int i = 0; i<lista.size(); i++){
                            if(lista.get(i).getNick().equals(partes[1])){
                                lista.get(i).setSocket(this.clienteServidor.getSocket());
                                if(lista.get(i).getPasswd().equals(partes[2])){
                                    this.clienteServidor.setNick(lista.get(i).getNick());
                                    this.clienteServidor.setPasswd(lista.get(i).getPasswd());
                                    lista.get(i).salida = this.clienteServidor.salida;
                                    lista.get(i).entrada = this.clienteServidor.entrada;
                                    this.clienteServidor.salida.println(LOGINOK);
                                    logger.trace("Loggin conrrecto del usuario "+lista.get(i).getNick());
                                    clienteServidor.salida.println("Bienvenido al Servidor de Simion.");
                                    servidor.enviarNicks();
                                    break;
                                }
                                else{
                                    this.clienteServidor.salida.println(ERRORPASSWD);
                                    logger.warn("Loggin incorrecto del usuario "+lista.get(i).getNick());
                                    break;
                                }
                            }
                        }
                        break;
                    case SINGUPOK:
                        if (servidor.getCliente(partes[1]) == null){
                            clienteServidor.setNick(partes[1]);
                            clienteServidor.setPasswd(partes[2]);
                            servidor.aniadirCliente(clienteServidor);
                            clienteServidor.salida.println(SINGUPOK);
                            clienteServidor.salida.println("Registrado como "+clienteServidor.getNick());
                            logger.trace("Usuario "+clienteServidor.getNick()+" registrado con exito.");
                            clienteServidor.salida.println("Bienvenido al Servidor de Simion.");
                            servidor.enviarNicks();

                        }
                        else {
                            clienteServidor.salida.println(REGISTRADO);
                            logger.warn("Error. Usuario ya registrado.");
                        }
                        break;
                    case IGNORAR:
                        ignorado = servidor.getCliente(partes[1]);
                        if (ignorado != null){
                            clienteServidor.ignorados.add(ignorado);
                            clienteServidor.salida.println("El usuario "+partes[1]+" será ignorado a partir de ahora.");
                            logger.trace("El usuario "+clienteServidor.getNick()+" ha ignorado a "+ignorado.getNick());
                        }
                        else {
                            clienteServidor.salida.println("El usuario "+partes[1]+" no existe.");
                            logger.trace("El usuario "+clienteServidor.getNick()+" ha proporcionado un nick no existente");
                        }
                        break;
                    case NOIGNORAR:
                        ignorado = servidor.getCliente(partes[1]);
                        if (ignorado != null){
                            clienteServidor.ignorados.remove(ignorado);
                            clienteServidor.salida.println("Ya puedes recibir mensajes del " +
                                    "usuario "+partes[1]+"  a partir de ahora.");
                            logger.trace("El usuario "+clienteServidor.getNick()+" ha admitido los mensajes de "+ignorado.getNick());
                        }
                        else {
                            clienteServidor.salida.println("El usuario "+partes[1]+" no existe.");
                            logger.trace("El usuario "+clienteServidor.getNick()+"ha suministrado un nick erroneo");
                        }
                        break;
                    case CONECTADO:
                        clienteServidor.salida.println(CONECTADO);
                        break;
                    case CHATPRIVADO:
                        if (!servidor.enviarMensajePrivado(partes[1], partes[2], clienteServidor)){
                            clienteServidor.salida.println(CHATPRIVADONO);
                        }
                        logger.trace("El usuario "+clienteServidor.getNick()+" ha enviado un mensaje.");
                        break;
                    case SALIR:
                        servidor.enviarMensaje("El usuario "+clienteServidor.getNick()+" se ha desconectado");
                        clienteServidor.desconectar();
                        servidor.enviarNicks();
                        logger.trace("El usuario "+clienteServidor.getNick()+" se ha desconectado");
                        break;
                    default:
                        servidor.enviarMensaje(orden, clienteServidor);
                        logger.trace("El usuario "+clienteServidor.getNick()+" ha enviado un mensaje.");
                        break;
                }
            }
        } catch (IOException e) {
            logger.trace("Error de entrada salida.");
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw);
        }
    }
}

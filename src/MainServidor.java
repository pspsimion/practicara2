import com.simion.base.ClienteServidor;
import com.simion.base.Servidor;
import com.simion.ConexionCliente;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.text.DateFormat;
import java.util.Date;

/**
 * Created by dam on 22/02/17.
 */
public class MainServidor {

    private static final Logger logger = LogManager.getLogger(MainServidor.class);

    public static void main(String[] args){
        Servidor servidor = new Servidor(5555);
        try{
            servidor.iniciar();
            logger.trace("Servidor Iniciado");
            while (servidor.estaConectado()){
                Socket socketCliente = servidor.aceptarConexion();
                ClienteServidor clienteServidor = new ClienteServidor(socketCliente);
                ConexionCliente conexionCliente = new ConexionCliente(servidor, clienteServidor);
                conexionCliente.start();
                logger.trace("Nueva conexion con un cliente. IP: "+socketCliente.getInetAddress().getAddress()
                        +" Puerto local:"+socketCliente.getLocalPort());
            }
        } catch (IOException e) {
            logger.trace("Se ha producido un error en la conexion del servidor.");
            StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            logger.error(sw.toString());
        }
    }
}
